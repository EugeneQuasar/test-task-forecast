import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/question2',
    name: 'question2',
    component: () => import('../views/question2.vue')
  },
  {
    path: '/question3',
    name: 'question3',
    component: () => import('../views/question3.vue')
  },
  {
    path: '/question4',
    name: 'question4',
    component: () => import('../views/question4.vue')
  },  
  {
    path: '/question5',
    name: 'question5',
    component: () => import('../views/question5.vue')
  },
  {
    path: '/call-page',
    name: 'call-page',
    component: () => import('../views/call-page.vue')
  },
  {
    path: '/userprofile',
    name: 'userprofile',
    component: () => import('../views/userprofile.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router